public class Calculator {

    private int result;

    public Calculator() {
        reset();
    }

    public void reset() {
        this.result = 0;
    }

    public int getResult() {
        return this.result;
    }

    public void add(int value) {
        this.result += value;
    }

    public void subtract(int value) {
        this.result -= value;
    }

    public void multiply(int value) {
        if (value == 0) {

            result = 0;
        } else {
            int temp = result;
            result = 0;
            for (int i = 1; i <= value; i++) {
                add(temp);
            }
        }
    }

    public void exponent(int value) {
        if (value == 0) {

            result = 1;
        } else if (result == 0 && value < 0) {

            throw new IllegalArgumentException("0 cannot be raised to a negative power");
        } else {
            int temp = result;
            for (int i = 1; i < value; i++) {
                multiply(temp);
            }
        }
    }
}
