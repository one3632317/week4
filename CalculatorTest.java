import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testAdd() {
        calculator.add(5);
        assertEquals(calculator.getResult(), 5);
    }

    @Test
    public void testSubtract() {
        calculator.add(10);
        calculator.subtract(5);
        assertEquals(calculator.getResult(), 5);
    }

    @Test
    public void testMultiply() {
        calculator.multiply(4);
        assertEquals(calculator.getResult(), 0);
    }

    @Test
    public void testExponent() {
        calculator.exponent(3);
        assertEquals(calculator.getResult(), 0);
    }
}
